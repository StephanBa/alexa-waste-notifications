from urllib.parse import urlencode
from urllib.request import Request, urlopen

class AWMRequest:
    
    def __init__(self, street):
        self.street = street
        self.url = 'http://abfuhrtermine.stadt-muenster.de/abfallkalender/awm/res/AwmStart.php' # Set destination URL here

    def requestAWM(self, params , fetchStreet = False):
        request = Request(self.url, urlencode(params).encode())
        json = urlopen(request).read().decode()
        print("Request success")
        try:
            startIndex = 'name="mm_ses" value="'
            startIndex = json.index(startIndex) + len(startIndex)
            endIndex = json.index('"', startIndex)
            sessionKey = json[startIndex : endIndex]
        except ValueError:
            sessionKey = json
            
        if(fetchStreet):
            try:
                startIndex = '<option selected="selected" value="'
                startIndex = json.index(startIndex) + len(startIndex)
                endIndex = json.index('"', startIndex)
                self.street = json[startIndex : endIndex]
            except ValueError:
                #Direct Match
                startIndex = '<div class="m_symadr">'
                startIndex = json.index(startIndex) + len(startIndex)
                startIndex = json.index("</strong><br />",startIndex) + len("</strong><br />")
                endIndex = json.index('<br />', startIndex)
                self.street = json[startIndex : endIndex]
        return sessionKey

    def runRequests(self):
        
        postFields = {}
        sessionKey = self.requestAWM(postFields)
        
        #1
        postFields = {
        'mm_ses': sessionKey
        ,
        'mm_aus_ort.x':'15'
        ,
        'mm_aus_ort.y':'23'
        }
        sessionKey = self.requestAWM(postFields)
        
        #2
        postFields = {
        'mm_ses': sessionKey
        ,
        'xxx': '1'
        ,
        'mm_frm_str_name': self.street
        ,
        'mm_aus_str_txt_submit':'suchen'
        }
        sessionKey = self.requestAWM(postFields,True)
        
        #3
        postFields = {
        'mm_ses': sessionKey
        ,
        'xxx': '1'
        ,
        'mm_frm_str_sel': self.street
        ,
        'mm_aus_str_sel_submit':'weiter'
        }
        sessionKey = self.requestAWM(postFields)
        
        #4
        postFields = {
        'mm_ses': sessionKey
        ,
        'xxx': '1'
        ,
        'mm_ica_auswahl' : 'iCalendar-Datei'
        }
        sessionKey = self.requestAWM(postFields)
        
        #5
        postFields = {
        'mm_ses': sessionKey
        ,
        'xxx': '1'
        ,
        'mm_frm_type' : 'termine'
        ,
        'mm_frm_fra_RM' : 'RM'
        ,
        'mm_frm_fra_PPK' : 'PPK'
        ,
        'mm_frm_fra_BIO' : 'BIO'
        ,
        'mm_frm_fra_GS' : 'GS'
        ,
        'mm_frm_fra_SPG' : 'SPG'
        ,
        'mm_ica_gen' : 'iCalendar-Datei laden'
        }
        icalResponse = self.requestAWM(postFields)
        
        return icalResponse
