from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^next/(?P<type>[\w-]+)$', views.StaticCalendarView.as_view(), name='next-event-static'),
    url(r'^next/$', views.RemoteCalenderView.as_view(), name='next-event-awm'),
]