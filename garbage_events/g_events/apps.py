from django.apps import AppConfig


class GEventsConfig(AppConfig):
    name = 'g_events'
