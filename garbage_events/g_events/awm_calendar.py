from .magic import AWMRequest


class StaticCalendar():

    @property
    def cal_stream(self):
        with open('/home/rednik96/Downloads/AWM_Abfuhrtermine_Stand_20170307.ICS') as cal:
            return cal.read()

class AWMCalendar():

    def __init__(self, street):
        self._street = street

    @property
    def cal_stream(self):
        resp = AWMRequest(self._street).runRequests()
        print('------------')
        print(resp)
        print('------------')
        return resp
    