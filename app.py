from flask import Flask
from flask_ask import Ask, statement, question, context, session
import logging
import requests
import datetime
import json
import os

app = Flask(__name__)
ask = Ask(app, '/')

log = logging.getLogger('flask_ask')
log.setLevel(logging.DEBUG)

@ask.intent('HelloIntent')
def hello(firstname):
    speech_text = "Hello %s" % firstname
    return statement(speech_text).simple_card('Hello', speech_text)

@ask.intent('AskAddressIntent')
def ask_address():
    speech_text = "Wie lautet deine Straße?"
    return question(speech_text).reprompt(speech_text)

@ask.intent('AddressIntent')
def address(address, housenumber):
    #speech_text = "Ok, ich habe deine Straße auf {} geändert. Wie lautet deine Hausnummer?".format(address)
    save_address(address, housenumber)
    speech_text = "Ok, ich habe deine Adresse auf {} {} geändert. Danke für dein Update!".format(address, housenumber)
    return question(speech_text).reprompt(speech_text)

@ask.intent('AddressHousenumberIntent')
def address(housenumber):
    log.debug(housenumber)
    log.debug(session.attribute)

    speech_text = "Ok, ich habe deine Adresse auf {} {} geändert. Danke für dein Update!".format(session.attributes['address'], housenumber)
    return statement(speech_text)

@ask.launch
def launch():
    speech_text = 'Willkommen zum Abfallkalender. Du kannst mich zum  Beispiel folgendes Fragen' \
                  '"Wann wird der Gelbe Sack abgeholt?" oder "Wann wird der Biomüll abgeholt?". Alternativ kannst du auch deine Adresse' \
                  'mit dem Befehl "neue Adresse Straßennamer Hausnummer" ändern'
    location = get_alexa_location()
    city = "Your City is {}! ".format(location["city"].encode("utf-8"))
    address = "Your address is {}! ".format(location["addressLine1"].encode("utf-8"))
    log.debug(city)
    log.debug(address)
    return question(speech_text).reprompt(speech_text).simple_card('HelloWorld', speech_text)

@ask.intent('WasteResponseIntent')
def waste_intent(waste_type):
    date = get_next_date_for_waste_type(waste_type)
    if date:
        return statement('Die nächste Abholung für {} ist am {}'.format(waste_type, date))
    else:
        return statement('Es gibt momentan leider keine Daten. Versuch es später nochmal')

@ask.on_session_started
def new_session():
    log.info('new session started')

def get_alexa_location():
    URL =  "https://api.eu.amazonalexa.com/v1/devices/{}/settings" \
           "/address".format(context.System.device.deviceId)
    TOKEN =  context.System.user.permissions.consentToken
    log.info(TOKEN)
    HEADER = {'Accept': 'application/json',
             'Authorization': 'Bearer {}'.format(TOKEN)}
    r = requests.get(URL, headers=HEADER)
    log.debug(r.status_code)
    log.debug(r.content)
    if r.status_code == 200:
        return(r.json())


def get_next_date_for_waste_type(waste_type):
    log.debug(waste_type)
    wtype = get_waste_type(waste_type)
    if not wtype:
        return None
    payload = {"type": wtype, "address": get_address()}
    content = requests.post('http://django:8080/events/next/', data=payload)
    log.debug(content.content)
    if content.status_code == 200:
        c_json = content.json()
        log.debug(c_json)
        date = datetime.datetime.strptime(c_json['start'], '%Y-%m-%d')
        return "{}.{}.{}".format(date.day, date.month, date.year)
    return None

def date_to_string(date):
    return date

def get_waste_type(waste_name):
    GARBAGE_TYPE = {
        'biomüll': 'bio',
        'gelbe sack': 'plastik',
        'restabfall': 'rest',
        'papier': 'papier',
        'sperrgut': 'sperrgut',
    }

    if waste_name in GARBAGE_TYPE:
        return GARBAGE_TYPE[waste_name]
    else:
        return None

def save_address(street, housenumber):
    device_id = context.System.device.deviceId
    file_directory = "./UserLocations/" + device_id + ".json"

    if os.path.exists('UserLocations') is False:
        os.mkdir('UserLocations')

    outfile = open(file_directory, 'w+')
    addressdata = {'address': [{'street': street,
                                'housenumber': housenumber
                            }]
                  }
    json.dump(addressdata, outfile)
    outfile.close()


def get_address():
    device_id = context.System.device.deviceId
    file_directory = "./UserLocations/" + device_id + ".json"

    if os.path.isfile(file_directory) is False:
        return None

    json_data = open(file_directory).read()
    data = json.loads(json_data)
    return "{} {}".format(data['address'][0]['street'], data['address'][0]['housenumber'])


short_months = [4, 6, 9, 11]
long_months = [1, 3, 5, 7, 8, 10, 12]


def get_days_from_date(date):
    returnValue = 0

    now = datetime.datetime.now()
    yearReq = int(date[0:4])
    monthReq = int(date[4:6])
    dayReq = int(date[6:8])

    currentYear = now.year
    currentMonth = now.month
    currentDay = now.day

    yearDif = yearReq - currentYear
    monthDif = monthReq - currentMonth
    dayDif = dayReq - currentDay

    if (yearDif > 0):
        if(monthReq > currentMonth):
            returnValue += 365*yearDif
        else:
            returnValue += 365*(yearDif-1)
    if (monthDif != 0):
        returnValue += get_days_from_month_dif(currentMonth, monthReq, yearReq)
    if (dayDif != 0):
        returnValue += dayDif
        return str(returnValue)


def get_days_from_month_dif(from_month, to_month, year_request):
    returnValue = 0
    while (from_month != to_month):
        if (from_month in short_months):
            returnValue += 30
        elif (from_month in long_months):
            returnValue += 31
        else:
            if ((year_request % 4) == 0):
                returnValue += 29
            else:
                returnValue += 28
        from_month = from_month % 12 + 1
    return returnValue

if __name__ == '__main__':
    app.run(host="0.0.0.0", debug=True)