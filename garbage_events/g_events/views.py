import json

from rest_framework.views import APIView
from rest_framework.response import Response

from .awm_calendar import StaticCalendar, AWMCalendar
from .garbage_parser import GarbageParser, LastReachedException


class StaticCalendarView(APIView):

    def get(self, request, type):
        stat_cal = StaticCalendar()
        gp = GarbageParser(stat_cal)

        try:
            ev = gp.get_next_by_type('bio')
            print('not raised')
        except LastReachedException:
            print('except')
            return Response({'error': 'Last event for this year was reached'}, 404)
        data = {
            'start': ev.start.isoformat(),
            'end': ev.end.isoformat(),
            'location': ev.location
        }
        return Response(data, status=200, content_type='application/json')

class RemoteCalenderView(APIView):

    def post(self, request):
        try:
            data = request.data
            street = data['address']
            garb_type = data['type']
        except (Exception, KeyError) as e:
            print(e)
            return Response({'error': 'Could not parse the Response'})

        awm_calendar = AWMCalendar(street)
        gp = GarbageParser(awm_calendar)

        try:
            ev = gp.get_next_by_type(garb_type)
            print('not raised')
        except LastReachedException:
            print('except')
            return Response({'error': 'Last event for this year was reached'}, 404)
        data = {
            'start': ev.start.isoformat(),
            'end': ev.end.isoformat(),
            'location': ev.location
        }
        return Response(data, status=200, content_type='application/json')
