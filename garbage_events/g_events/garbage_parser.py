from icalendar import Calendar

GARBAGE_TYPE = {
    'bio': 'AWM Biotonne',
    'plastik': 'AWM Gelber Sack',
    'rest': 'AWM Restabfalltonne',
    'papier': 'AWM Papiertonne',
    'sperrgut': 'AWM Sperrgut',
}


class GarbageParser(object):
    """ Parser fuer .ical Dateien """

    def __init__(self, file_server):
        """ Konstruktor basierend auf einem Dateipfad, es soll auch einen geben, der auf Datei-Basis funktioniert """
        self._ical = Calendar.from_ical(file_server.cal_stream)

    def __iter__(self):
        for component in self._ical.walk():
            if component.name == 'VEVENT':
                yield component

    def get_next_by_type(self, garbage_type):
        garbage_type = GARBAGE_TYPE[garbage_type]
        event = None

        for comp in self:
            if comp.name == 'VEVENT' and comp.get('summary') == garbage_type:
                return CalEvent(comp)

        raise LastReachedException('last Event already reached')


class CalEvent(object):
    """ Wrapper für components von ical """

    def __init__(self, comp):
        """ strikter Konstruktor, es fehlt eine freundliche Variante """
        start = comp.get('dtstart', None)
        end = comp.get('dtend', None)
        location = comp.get('location', None)
        garbage_type = comp.get('summary', None)

        if start and end and location and garbage_type:
            self._start = start
            self._end = end
            self._location = location
            self._garbage_type = garbage_type

    @property
    def start(self):
        return self._start.dt

    @property
    def end(self):
        return self._end.dt

    @property
    def location(self):
        return self._location

    @property
    def garbage_type(self):
        return self._garbage_type


class LastReachedException(Exception):
    pass




if __name__ == '__main__':
    gp = GarbageParser('/home/rednik96/Downloads/AWM_Abfuhrtermine_Stand_20170307.ICS')
    ev = gp.get_next_by_type('bio')
    print(ev.start)
    print(ev.end)
    print(ev.location)
    print(ev.garbage_type)
